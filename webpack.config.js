const path = require('path');

module.exports = options => {
    return {
        entry: './src/index.js',
        output: {
            filename: 'bundle.js',
            path: '/Users/jjlendaya/development/js-learning/react/todo-list/public/'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                cacheDirectory: true
                            }
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                }
            ]
        },
        devServer: {
            contentBase: path.join(__dirname, 'public')
        }
    }
};
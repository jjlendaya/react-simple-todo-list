import React from 'react';
import TodoItem from './TodoItem';
import TaskForm from './TaskForm';
import localForage from 'localforage';
import './App.css';

class App extends React.Component {

    constructor() {
        super();

        this.state = {
            tasks: [],
            formValue: '',
            todoCounter: 0
        };

        localForage.getItem('state').then(value => {
            if (value != null) {
                this.setState({
                    ...value
                })
            }
        });

        this.handleToggleCompleted = this.handleToggleCompleted.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.handleTaskSubmit = this.handleTaskSubmit.bind(this);
        this.saveState = this.saveState.bind(this);
        this.handleRemoveButtonPress = this.handleRemoveButtonPress.bind(this);
    }

    static createNewTodo(content, todoCounter) {
        return {
            completed: false,
            id: todoCounter,
            content
        };
    }

    saveState() {
        localForage.setItem('state', this.state);
    }

    handleToggleCompleted(idToSet) {
        let tasks = this.state.tasks.slice();
        tasks.map((element) => {
            if (element.id === idToSet) {
                element['completed'] = !element['completed'];
            }
            return element;
        });
        this.setState({tasks});
    };

    handleFormChange(event) {
        this.setState({
            formValue: event.target.value
        });
    }

    handleTaskSubmit(event) {
        event.preventDefault();
        const todoCounter = this.state.todoCounter + 1;
        let tasks = this.state.tasks.slice();
        tasks.push(App.createNewTodo(this.state.formValue, todoCounter));
        this.setState({
            tasks,
            todoCounter,
            formValue: '',
        });
        
    }

    handleRemoveButtonPress(idToRemove) {
        let index = this.state.tasks.findIndex((element) => {
            return element.id === idToRemove
        });
        let tasks = this.state.tasks.slice(0, index).concat(this.state.tasks.slice(index + 1))
        this.setState({tasks})
    }

    render() {
        this.saveState();

        return (
            <div className='root'>
                <div className='header'>
                    <h1>Tasks</h1>
                </div>
                <div className='task-container'>
                    {this.state.tasks.map(({completed, content, id}) =>
                        <TodoItem
                            id={id}
                            completed={completed}
                            content={content}
                            key={id}
                            toggleCompleted={this.handleToggleCompleted}
                            handleButtonPress={this.handleRemoveButtonPress}
                        />
                    )}
                </div>
                <TaskForm
                    formValue={this.state.formValue}
                    handleFormChange={this.handleFormChange}
                    handleSubmit={this.handleTaskSubmit}
                />
            </div>
        );
    };
}

export default App;
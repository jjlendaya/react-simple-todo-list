import React from 'react';
import PropTypes from 'prop-types';
import './TodoItem.css';


function TodoItem({id, completed, content, toggleCompleted, handleButtonPress}) {
    return (
        <div>
            <div className='checkBox' onClick={() => toggleCompleted(id)}>
                <input id={`checkbox${id}`} type='checkbox' checked={completed} />
                <label htmlFor={`checkbox${id}`} onClick={event => event.stopPropagation()}></label>
            </div>
            <div style={{flexGrow: 2}}><p>{content}</p></div>
            <div>
                <a href=''
                   onClick={(event) => {
                       event.preventDefault();
                       handleButtonPress(id);
                   }}>x
                </a>
            </div>
        </div>
    )
}

TodoItem.propTypes = {
    completed: PropTypes.bool.isRequired,
    content: PropTypes.string.isRequired,
    toggleCompleted: PropTypes.func.isRequired
};

export default TodoItem;
import React from 'react';
import './TaskForm.css';


function TaskForm(props) {
    return (
        <form onSubmit={props.handleSubmit} className='task-form'>
            <input
                type="text"
                placeholder="Type a task and press enter..."
                value={props.formValue}
                onChange={props.handleFormChange} />
        </form>
    )
}

export default TaskForm;
